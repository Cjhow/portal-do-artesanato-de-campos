<?php include('header.php'); ?>

<!--AGENDAS E EVENTOS!-->

<div id="artesaos">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Artesãos</h1>
                <p></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <img src="image/imageperfil.jpg" class="profileimage">
            </div>
            <div class="col-md-8">
                <h5>Artesã</h5>
                <h2>Conceição Fernandes</h2>
                <h5>Técnicas</h5>
                <h4>Técnicas Diversas</h4>
                <h5>Materiais</h5>
                <h4>Materiais Diversos</h4>
                <h5>Informações Complementares</h5>
                <h4>Informações Diversas</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 artesaotext">
                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                    aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.”</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="image/Imageslide.jpg" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="image/Imageslide.jpg" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="image/Imageslide.jpg" alt="Third slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <h5>Produto</h5>
                <h4>Nome do Produto</h4>
            </div>
        </div>
        <div class="row produtos-artesaos">
            <div class="col-md-2">
            </div>
            <div class="col-md-2">
                <a href="">
                    <img src="image/produtoartesao.jpg" class="">
                </a>
            </div>
            <div class="col-md-2">
                <a href="#">
                    <img src="image/produtoartesao.jpg" class="">
                </a>
            </div>
            <div class="col-md-2">
                <a href="#">
                    <img src="image/produtoartesao.jpg" class="">
                </a>
            </div>
            <div class="col-md-2">
                <a href="#">
                    <img src="image/produtoartesao.jpg" class="">
                </a>
            </div>
            <div class="col-md-2">
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 text-right">
                <img id="wpp" src="image/wpplogo.png">
            </div>
            <div class="col-md-8 text-center">
                <form>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1"><h5> Gostou das peças? Entre em contato com a artesã. Envie sua mensagem</h5></label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="5"></textarea>
                    </div>
                </form>
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div>
</div>
<div id="produtos">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="artesao-title">Outros Artesãos</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="card mx-auto">
                    <img src="image/produto.jpg" class="card-img-top" alt="...">
                    <div class="card-body ">
                        <h2 class="card-title">Conceição Fernandes</h2>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mx-auto">
                    <img src="image/produto.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h2 class="card-title">Conceição Fernandes</h2>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mx-auto">
                    <img src="image/produto.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h2 class="card-title">Conceição Fernandes</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div id="agendaeventos">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center agenda-eventos">AGENDA DE EVENTOS</h3>
                <ul class="timeline">
                    <li class="timeline-inverted">
                        <div class="timeline-badge warning">
                            <span class="date-day">01<span class="date-month">05</span></span>
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                            </div>
                            <div class="timeline-body">
                                <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem
                                    é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge warning">
                            <span class="date-day">01<span class="date-month">05</span></span>
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                            </div>
                            <div class="timeline-body">
                                <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem
                                    é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div id="btn-cadastro" class="row">
            <div class="col-md-12 text-center">
                <a href="#" class="btn btn-info btn-lg">Cadastre Seu Evento</a>
            </div>
        </div>
    </div>
</div>

<div id="banner">
    <img class="img-fluid" src="image/logo2.jpg">
</div>

<!--Inicio Card Inferior-->

<div id="videocardinf">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-6">
                <h2>Vídeos</h2>
                <div class="embed-responsive embed-responsive-16by9 video-card">
                    <iframe src="https://www.youtube.com/embed/SC1XE85BC9o" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-12 col-lg-6">
                <h2>Instagram</h2>
                <div class="row">
                    <div class="col-md-4">
                        <img src="">
                    </div>
                    <div class="col-md-4">
                        <img src="">
                    </div>
                    <div class="col-md-4">
                        <img src="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <img src="">
                    </div>
                    <div class="col-md-4">
                        <img src="">
                    </div>
                    <div class="col-md-4">
                        <img src="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>