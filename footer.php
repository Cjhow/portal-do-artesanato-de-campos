<!--Footer!-->

<footer id="page-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
               <a href="index.php"> <img src="image/logofooter.png" id="logofooter"></a>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-5 col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="list-unstyled ">
                                    <li>
                                        <h5>Projeto</h5>
                                    </li>
                                    <li><a href="o-projeto.php">- O Projeto</a></li>
                                    <li><a href="#">- Calendário de Eventos</a></li>
                                    <li><a href="cadastro-do-artesao.php">- Cadastro Artesão</a></li>
                                    <li><a href="#">- Cadastro Evento</a></li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="list-unstyled">
                                    <li>
                                        <h5>Legislação</h5>
                                    </li>
                                    <li><a href="lei-do-artesao.php">- Lei do Artesão</a></li>
                                    <li><a href="#">- Plano do Artesão</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <ul class="list-unstyled">
                                    <li>
                                        <h5>Notícias</h5>
                                    </li>
                                    <li><a href="todas-noticias.php">- Últimas notícias</a></li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <ul class="list-unstyled">
                                    <li>
                                        <h5>Vitrine</h5>
                                    </li>
                                    <li><a href="produtos.php">- Produtos</a></li>
                                    <li><a href="artesaos.php">- Artesãos</a></li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <ul class="list-unstyled">
                                    <li>
                                        <h5>Contato</h5>
                                    </li>
                                    <li><a href="#">- Fale Conosco</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="js/jquery.min.js "></script>
<script src="js/pooper.min.js "></script>
<script src="js/bootstrap.min.js "></script>
</body>

</html>