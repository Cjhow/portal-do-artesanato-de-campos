<!DOCTYPE html>
<html>

<head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/fonts.min.css">


    <title>Portal do Artesanato de Campos</title>

</head>

<body>
    <header class="main-header">
        <div class="container" id="container-header">
            <div class="row">
                <div class="col-md-4">
                    <a href="index.php">
                        <img src="image/logoportal.jpg" id="logo">
                    </a>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <button class="btn btn-lg btn-block btn-header">
                                <h4>Pré-Cadastro Carteirinha</h4>
                            </button>
                        </div>
                        <div class="col-md-6">
                            <a class="btn btn-lg btn-block btn-header" href="cadastro-do-artesao.php">
                                <h4>Cadastro - Artesão</h4>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <nav class="navbar navbar-expand-lg">
                                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                                    <ul class="navbar-nav">
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Projeto
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                <a class="dropdown-item" href="o-projeto.php">O PROJETO</a>
                                                <a class="dropdown-item" href="#">CALENDARIO DE EVENTOS</a>
                                                <a class="dropdown-item" href="cadastro-do-artesao.php">CADASTRO ARTESAO</a>
                                                <a class="dropdown-item" href="#">CADASTRO EVENTO</a>
                                            </div>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        LEGISLAÇÃO
                                    </a>
                                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                <a class="dropdown-item" href="lei-do-artesao.php">LEI DO ARTESÃO</a>
                                                <a class="dropdown-item" href="#">PLANO ARTESÃO</a>
                                            </div>
                                        </li>
                                        <li class="nav-item">
                                            <a class="navbar" href="todas-noticias.php">NOTÍCIAS</a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        VITRINE
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                <a class="dropdown-item" href="produtos.php">PRODUTOS</a>
                                                <a class="dropdown-item" href="artesaos.php">ARTESÃOS</a>
                                            </div>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="navbar" href="#">CONTATO</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>