<?php include('header.php'); ?>

<!--AGENDAS E EVENTOS!-->

<div id="leiartesao">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Lei do Artesão - Lei Nº 13.180</h1>
                <p><a href="">Baixe o arquivo aqui.</a><br>Essa lei dispõe sobre a profissão do artesão e outras providências.
                </p>

                <h1>Base Conceitual Do Artesanato Brasileiro</h1>
                <p><a href="">Baixe o arquivo aqui.</a><br>PAM - Programa do Artesanato Brasileiro
                </p>
            </div>
        </div>
    </div>
</div>

<div id="agendaeventos">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center agenda-eventos">AGENDA DE EVENTOS</h3>
                <ul class="timeline">
                    <li class="timeline-inverted">
                        <div class="timeline-badge warning">
                            <span class="date-day">01<span class="date-month">05</span></span>
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                            </div>
                            <div class="timeline-body">
                                <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem
                                    é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge warning">
                            <span class="date-day">01<span class="date-month">05</span></span>
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                            </div>
                            <div class="timeline-body">
                                <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem
                                    é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div id="btn-cadastro" class="row">
            <div class="col-md-12 text-center">
                <a href="#" class="btn btn-info btn-lg">Cadastre Seu Evento</a>
            </div>
        </div>
    </div>
</div>

<div id="banner">
    <img class="img-fluid" src="image/logo2.jpg">
</div>

<!--Inicio Card Inferior-->

<div id="videocardinf">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-6">
                <h2>Vídeos</h2>
                <div class="embed-responsive embed-responsive-16by9 video-card">
                    <iframe src="https://www.youtube.com/embed/SC1XE85BC9o" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-12 col-lg-6">
                <h2>Instagram</h2>
                <div class="row">
                    <div class="col-md-4">
                        <img src="">
                    </div>
                    <div class="col-md-4">
                        <img src="">
                    </div>
                    <div class="col-md-4">
                        <img src="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <img src="">
                    </div>
                    <div class="col-md-4">
                        <img src="">
                    </div>
                    <div class="col-md-4">
                        <img src="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>