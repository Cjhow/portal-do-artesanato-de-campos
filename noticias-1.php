<?php include('header.php'); ?>

<!--Notícias-->

<div id="noticiaspage">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Notícias</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <h2>"Arena Cultura estreia no Alô Farol 2019 com oficina de artesanato"</h2>
                <h5>Publicado por: Fulano de Tal Foto: Divulgação Data: 18/01/2019 - 00:00h</h5>
                <img src="image/imagemnot3.jpg" class="noticias-img">
                <p>A Fundação Cultural Jornalista Oswaldo Lima (FCJOL) estreou na manhã de quinta-feira (16) a Arena Cultura na praia do Farol de São Thomé, com oficina de artesanato. A programação variada no local se estenderá até o dia 1º de março, na
                    Aldeia do Sol. Durante sete semanas do Verão Alô Farol 2019, moradores e veranistas terão acesso a oficinas de artesanato, mostras de dança, sessões de cinema, teatro, literatura e muita música. Nesta quinta (17) e sexta-feira (18)
                    tem mais oficina de artesanato e, no final de semana, oficina de origami, exposição de artesanato, Café Literário e doação de peixinhos. Veja AQUI programação completa. De acordo com o vice-presidente da FCJOL, Viny Soares, a intenção
                    é que as pessoas prestigiem as atrações e ocupem o espaço da Arena Cultura. — Pensamos em um local onde, durante a semana, todos possam interagir com a cultura e criamos um roteiro diferenciado, mas que complementa a programação de
                    verão do Farol. Queremos que os moradores e veranistas levem suas cadeiras e sintam-se à vontade para apreciar as atrações — disse o idealizador do projeto. Nesta primeira semana, haverá oficina de artesanato até esta sexta (18), de
                    9h às 11h. Já no sábado, tem a oficina de origami, exposição de artesanato e doação de peixes pela Peixinhos e Cia, que ocorre também no domingo pela manhã. Durante as sete semanas haverá ainda oficinas e exposições de artesanatos,
                    interações poéticas, homenagens a grandes nomes da nossa literatura, teatro adulto e infantil, exposição fotográfica recontando os 75 anos da Associação de Imprensa de Campos (AIC), e homenagens a grandes nomes da música como Cássia
                    Eller, Alcione, ao campista Jorge da Paz Almeida, entre outros. A Arena Cultura funcionará de quarta a domingo, sendo os eventos principais nas quartas, quintas e sextas-feiras, com cinema adulto e infantil, dança, teatro e shows.
                    Esta é a sétima arena montada no Farol para a temporada de verão. Os veranistas curtem também a Arena Show, Arena Esporte, Arena Radical, Arena Áqua, Arena Kids e Arena Futmesa.
                </p>
            </div>
            <div class="col-md-4">
            </div>
        </div>
    </div>
</div>




<div id="agendaeventos">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center agenda-eventos">AGENDA DE EVENTOS</h3>
                <ul class="timeline">
                    <li class="timeline-inverted">
                        <div class="timeline-badge warning">
                            <span class="date-day">01<span class="date-month">05</span></span>
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                            </div>
                            <div class="timeline-body">
                                <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem
                                    é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge warning">
                            <span class="date-day">01<span class="date-month">05</span></span>
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                            </div>
                            <div class="timeline-body">
                                <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem
                                    é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div id="btn-cadastro" class="row">
            <div class="col-md-12 text-center">
                <a href="#" class="btn btn-info btn-lg">Cadastre Seu Evento</a>
            </div>
        </div>
    </div>
</div>

<div id="banner">
    <img class="img-fluid" src="image/logo2.jpg">
</div>

<!--Inicio Card Inferior-->

<div id="videocardinf">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-6">
                <h2>Vídeos</h2>
                <div class="embed-responsive embed-responsive-16by9 video-card">
                    <iframe src="https://www.youtube.com/embed/SC1XE85BC9o" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-12 col-lg-6">
                <h2>Instagram</h2>
                <div class="row">
                    <div class="col-md-4">
                        <img src="">
                    </div>
                    <div class="col-md-4">
                        <img src="">
                    </div>
                    <div class="col-md-4">
                        <img src="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <img src="">
                    </div>
                    <div class="col-md-4">
                        <img src="">
                    </div>
                    <div class="col-md-4">
                        <img src="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>