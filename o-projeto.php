<?php include('header.php'); ?>

<div id="oprojeto">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Conheça o Projeto</h1>
                <p>O programa Viva o Artesanato, idealizado pela Companhia de Desenvolvimento do Município de Campos (CODEMCA) possui a finalidade de fomentar o artesanato e os processos de artes manuais, criando condições para agregar valor aos produtos,
                    buscando tornar Campos dos Goytacazes, uma das referências regionais nesse segmento, que apresenta-se como uma atividade econômica criativa, que possui forte potencial para a geração de renda e estímulo ao desenvolvimento justo, fortalecendo
                    grupos e comunidades. A história de uma região não se constrói apenas através de documentos oficiais, tendo o artesanato e as artes manuais o papel de perpetuação das tradições locais, preservando a identidade cultural dos diversos
                    grupos étnicos que formam a nossa sociedade. O projeto tem como objetivo central articular e otimizar o segmento, impulsionando a estratégia de economia circular, estabelecendo relações com outros setores produtivos e implantando ações
                    inovadoras, sustentáveis e empreendedoras, buscando o fortalecimento da atividade, para criar condições, a médio e longo prazo, para que Campos possa viver para além dos royalties.
                </p>
            </div>
        </div>
    </div>
</div>

<div id="agendaeventos">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center agenda-eventos">AGENDA DE EVENTOS</h3>
                <ul class="timeline">
                    <li class="timeline-inverted">
                        <div class="timeline-badge warning">
                            <span class="date-day">01<span class="date-month">05</span></span>
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                            </div>
                            <div class="timeline-body">
                                <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem
                                    é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge warning">
                            <span class="date-day">01<span class="date-month">05</span></span>
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                            </div>
                            <div class="timeline-body">
                                <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem
                                    é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div id="btn-cadastro" class="row">
            <div class="col-md-12 text-center">
                <a href="#" class="btn btn-info btn-lg">Cadastre Seu Evento</a>
            </div>
        </div>
    </div>
</div>

<div id="banner">
    <img class="img-fluid" src="image/logo2.jpg">
</div>

<!--Inicio Card Inferior-->

<div id="videocardinf">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-6">
                <h2>Vídeos</h2>
                <div class="embed-responsive embed-responsive-16by9 video-card">
                    <iframe src="https://www.youtube.com/embed/SC1XE85BC9o" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-12 col-lg-6">
                <h2>Instagram</h2>
                <div class="row">
                    <div class="col-md-4">
                        <img src="">
                    </div>
                    <div class="col-md-4">
                        <img src="">
                    </div>
                    <div class="col-md-4">
                        <img src="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <img src="">
                    </div>
                    <div class="col-md-4">
                        <img src="">
                    </div>
                    <div class="col-md-4">
                        <img src="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>

</html>